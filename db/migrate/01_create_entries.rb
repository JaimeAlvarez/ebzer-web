class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.integer :device_id, null: false
      t.string :full_image_url, null: false
      t.string :plate_segment_url, null: false
      t.string :plate_chars
      t.timestamp :creation_date_at_device, null: false
      t.integer :entry_type

      t.timestamps null: false
    end
  end
end