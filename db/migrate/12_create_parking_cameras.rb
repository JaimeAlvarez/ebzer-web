class CreateParkingCameras < ActiveRecord::Migration
  def change
    create_table :parking_cameras do |t|
      t.string :user
      t.string :password
      t.string :ip
      t.string :port
      t.string :type_of
      t.string :description
      t.boolean :state
      t.string :protocol
      
      t.timestamps null: false
    end
  end
end
