class CreateConfManagers < ActiveRecord::Migration
  def change
    create_table :conf_managers do |t|
      t.string :ip
      t.string :mask
      t.string :gateway
      t.string :dns_one
      t.string :dns_two
      
      t.timestamps null: false
    end
  end
end
