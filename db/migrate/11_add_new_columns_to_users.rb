class AddNewColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :created_by, :integer
    add_column :users, :is_active, :boolean, :default => true
  end
end
