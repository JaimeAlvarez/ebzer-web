class AddBrandsToRefCamera < ActiveRecord::Migration
  def change
  	add_reference :ref_cameras, :brand, index: true
  end
end
