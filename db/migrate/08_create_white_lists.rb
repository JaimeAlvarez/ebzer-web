class CreateWhiteLists < ActiveRecord::Migration
  def change
    create_table :white_lists do |t|
      t.string :plate
      t.string :reason

      t.timestamps null: false
    end
  end
end
