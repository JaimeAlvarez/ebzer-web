class AddRefCameraToParkingCamera < ActiveRecord::Migration
  def change
   add_reference :parking_cameras, :ref_camera, index: true
  end
end
