class CreateRefCameras < ActiveRecord::Migration
  def change
    create_table :ref_cameras do |t|
		t.string :reference
		t.string :path
		t.string :protocol
		t.string :type_of
		t.timestamps null: false
    end
  end
end
