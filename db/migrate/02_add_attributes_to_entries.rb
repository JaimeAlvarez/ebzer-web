class AddAttributesToEntries < ActiveRecord::Migration
  def change
  	add_column :entries, :edited, :boolean
  	add_column :entries, :entry_related_id, :integer
  	add_column :entries, :time_related, :timestamp
  end
end