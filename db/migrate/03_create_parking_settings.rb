class CreateParkingSettings < ActiveRecord::Migration
  def change
    create_table :parking_settings do |t|
      t.string :name, null: false
      t.integer :maximum_capacity, null: false
      t.text :observations
      t.integer :number_of_entries
      t.integer :available_capacity
      t.integer :number_of_cameras 
      
      t.timestamps null: false
    end
  end
end
