class AddAttributesToParkingSettingsOne < ActiveRecord::Migration
  def change
  	add_column :parking_settings, :owner, :string
  	add_column :parking_settings, :location, :string
  end
end