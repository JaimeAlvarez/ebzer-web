ParkingSetting.create(name: 'Ebenezer parking', number_of_entries: 0, owner: "Ebenezer", location: "Barranquilla", observations: "ZER", number_of_cameras: 6)
User.create(full_name: "Juan", colombian_id: "1234567890", role: "admin",password: "12345678", email: "info@ebenezertechs.com")
#,crypted_password: "$2a$10$JZEXvKDmGpA.tTLvr7Et.u3C8oE6UiuByTFn59O4TKAyRAmUCe.Gu"
ConfManager.create(ip: "192.168.1.114", mask: "255.255.255.1",gateway: "192.168.1.114" ,dns_one: "8.8.8.8", dns_two: "8.8.4.4")
Brand.create(brand: "sony")
RefCamera.create(reference: "yh900", path: "/Streaming/Channels/2", brand_id: "1")
Brand.create(brand: "lg")
RefCamera.create(reference: "lg100k", path: "/Streaming/Channels/2", brand_id: "2")
