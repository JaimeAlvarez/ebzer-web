# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 17) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "black_lists", force: :cascade do |t|
    t.string   "plate",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "reason"
  end

  create_table "brands", force: :cascade do |t|
    t.string   "brand"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conf_managers", force: :cascade do |t|
    t.string   "ip"
    t.string   "mask"
    t.string   "gateway"
    t.string   "dns_one"
    t.string   "dns_two"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "entries", force: :cascade do |t|
    t.integer  "device_id",               null: false
    t.string   "full_image_url",          null: false
    t.string   "plate_segment_url",       null: false
    t.string   "plate_chars"
    t.datetime "creation_date_at_device", null: false
    t.integer  "entry_type"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.boolean  "edited"
    t.integer  "entry_related_id"
    t.datetime "time_related"
    t.boolean  "edit"
    t.string   "image"
  end

  create_table "parking_cameras", force: :cascade do |t|
    t.string   "user"
    t.string   "password"
    t.string   "ip"
    t.string   "port"
    t.string   "type_of"
    t.string   "description"
    t.boolean  "state"
    t.string   "protocol"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "ref_camera_id"
  end

  add_index "parking_cameras", ["ref_camera_id"], name: "index_parking_cameras_on_ref_camera_id", using: :btree

  create_table "parking_settings", force: :cascade do |t|
    t.string   "name",               null: false
    t.integer  "maximum_capacity",   null: false
    t.text     "observations"
    t.integer  "number_of_entries"
    t.integer  "available_capacity"
    t.integer  "number_of_cameras"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "owner"
    t.string   "location"
  end

  create_table "ref_cameras", force: :cascade do |t|
    t.string   "reference"
    t.string   "path"
    t.string   "protocol"
    t.string   "type_of"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "brand_id"
  end

  add_index "ref_cameras", ["brand_id"], name: "index_ref_cameras_on_brand_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "full_name",                             null: false
    t.string   "colombian_id",                          null: false
    t.string   "role",                                  null: false
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "created_by"
    t.boolean  "is_active",              default: true
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "white_lists", force: :cascade do |t|
    t.string   "plate"
    t.string   "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
