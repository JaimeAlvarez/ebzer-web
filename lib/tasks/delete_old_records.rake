#encoding: utf-8
namespace :delete do
	desc 'Borra datos mayores a 30 días.'
	task :old_records => :environment do 
		Entry.where('creation_date_at_device < ?', 30.days.ago).each do |entry|
			File.delete("/home/ebenezer/Documents/Jimmy/ebzer-web/public/photos/"+entry.full_image_url)
			File.delete("/home/ebenezer/Documents/Jimmy/ebzer-web/public/photos/"+entry.plate_segment_url)
			entry.delete
		end
	end
end