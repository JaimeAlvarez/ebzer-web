module SettingHelper

	def network_conf(ip,mask,gateway,dns_one,dns_two)

	    system("sudo rm /etc/network/interfaces")
	    archivo = File.open("/home/odroid/interfaces", "w+")
	    archivo.puts "auto lo"
	    archivo.puts "iface lo inet loopback"
	    archivo.puts "###------->"
	    archivo.puts "auto eth0"
	    archivo.puts "iface eth0 inet static"
	    archivo.puts "address #{ip}"
	    archivo.puts "netmask #{mask}"
	    archivo.puts "gateway #{gateway}"
	    archivo.puts "dns-nameservers #{dns_one}"
	    archivo.puts "dns-nameservers #{dns_two}"
	    archivo.close()
	    system("sudo mv /home/odroid/interfaces /etc/network/")	
	end

	def find_device
		output = `lsblk`
		archivo = File.open("lsblk.txt","w+")
		archivo.puts output
		archivo.close()

		archivo = File.open("lsblk.txt","r")
		$ARRAY = archivo.readlines
		archivo.close()
		return $ARRAY
	end

end
