$(document).ready(function(){

  $('#new_white_list').submit(function(){
    var plate = $('#white_list_plate').val();
    var go = "false";
        
    if(plate.match(/^\s*$/))
    {
      $('#nueva_placa_mensaje').html('- Placa no puede estar vacía.');
      return false;
    }else{
      if(!plate.match(/^[A-Z]{3}[0-9]{3}$/))
      {
        $('#nueva_placa_mensaje').html('- Placa inválida.');
        return false; 
      }else{        
        $.ajax({
          async: false,
          type: "POST",
          url: "/white_lists/search_b",
          data: {id: plate},
          success: function(json){
            var response = $.parseJSON(json);
            //alert(response);
            if(response=="1"){
              var conf = confirm("ATENCIÓN: esta placa se encuentra en Lista Negra. ¿Desea cambiarla a Lista Blanca?");
              if(conf==true)
              {
                $('#white_list_search_b').val('true');
                go = "true";
              }
            }
            else
            {
              $('#white_list_search_b').val('false');
              go = "true";
            }
          }
        });
      }
    }
    
    if(go=="true")
    {
      return true;
    }else{
      return false;
    }

  });

	$(document).on('ajax:success', '#new_white_list', function(event, data){
    $('#white_list_plate').val('');
    $('#white_list_motivo').val('');
    location.reload();
  });

  $('#white_list_plate').on('focus',function(){
    $('#nueva_placa_mensaje').html('');
    $('#white_list_plate').val('');
  });

  $('#agregar_btn').on('click', function(){
    $('#nueva_placa_mensaje').html('');
    $('#white_list_plate').val('');
  });
  
  $(document).on('ajax:error', '#new_white_list', function(event, data){
    alert(data.responseText);
    console.log(data);
  });

  $('#continue').click(function(){
    $(".white_lists:checked").each(function(){
      var id = $(this).val();
      $.ajax({
        async: false,
        type: "POST",
        url: "/white_lists/destroy",
        data: {id: id},
        success: function(json){
        }
      });  
    });
    location.reload();
  });
  
  $('#white_list_plate').keyup(function(){
    this.value = this.value.toUpperCase();
  });

  $('#buscar_placa_input').keyup(function(){
    this.value = this.value.toUpperCase();
  });

  $('#buscar_placa_input').on('focus',function(){
    $('#buscar_placa_mensaje').html('');
    $('#buscar_placa_input').val('');
  });

  $('#buscar_placa_button').click(function(){
    $('#buscar_placa_mensaje').html('');
    $('#buscar_placa_input').val('');
  });


  $('#buscar_placa_form').submit(function(){
    var plate = $('#buscar_placa_input').val();         
    if(plate.match(/^\s*$/))
    {
      $('#buscar_placa_mensaje').html('- Placa no puede estar vacía.');
      return false;
    }else{
      if(!plate.match(/^[A-Z]{3}[0-9]{3}$/))
      {
        if(!plate.match(/^[A-Z]{3}[0-9]{2}[A-Z]{1}$/))
        {
          $('#buscar_placa_mensaje').html('- Placa inválida.');
          return false;
        }else{
          return true;
        }  
      }else{
        return true;
      }
    }
  });

  $(document).on('ajax:success', '#buscar_placa_form', function(event, data){
    $('#placa_encontrada').html(data.plate);
    $('#editar_placa_input').val(data.plate);
    $('#motivo_placa_encontrada').html(data.motivo);
    $('#editar_motivo_input').val(data.motivo);
    $('#id_placa_encontrada').attr('value', data.id);
    $('#id_placa_edit').attr('value', data.id);
    $('#placaencontrada').foundation('reveal', 'open');
    console.log(data);
  });
  
  $(document).on('ajax:error', '#buscar_placa_form', function(event, data){
    alert(data.responseText);
    console.log(data);
  });

  $('#borrar_placa_encontrada').click(function(){
      var id = $('#id_placa_encontrada').val();
      $.ajax({
        async: false,
        type: "POST",
        url: "/white_lists/destroy",
        data: {id: id},
        success: function(json){
        }
      });
    location.reload(true);
  });

  $(document).on('ajax:success', '#editar_placa_encontrada_form', function(event, data){
    $('#editar_placa_input').val('');
    $('#editar_motivo_input').val('');
    $('#id_placa_edit').attr('value', '');
    alert("Placa editada satisfactoriamente.")
    location.reload();
  });

  $(document).on('ajax:error', '#editar_placa_encontrada_form', function(event, data){
    alert(data.responseText);
    console.log(data);
  });


});
