var id_cam = 0, type_of, description, i = 1, valueCamera, url = "", localhost = "";
var result, aux;
var path = "", name_device = "";

var getIdCamera = function(id){
  id_cam = id;
  i = 0;
}

var save_camera = function(id){
  
  //Variables de identificacion de elementos del html
  var htmlDesc = "description"+id, htmlUser = "user"+id, htmlPassword="password"+id;
  var htmlPort = "port"+id, htmlIp = "ip"+id, htmlState = "state"+id, htmlTypeOfOption = "type_of_option"+id;
  var htmlProtocol = "protocol"+id, htmlCamera = "camera"+id;
  //--

  //Variables que toman el valor de los campos de entada de texo html
  var description = document.getElementById(htmlDesc).value; //Descripcion
  var user = document.getElementById(htmlUser).value;//Usuario
  var password = document.getElementById(htmlPassword).value;//Contraseña
  var s = document.getElementById(htmlState);//Estado
  var ip = document.getElementById(htmlIp).value;//Ip
  var port = document.getElementById(htmlPort).value;//Puerto
  var protocol = document.getElementById(htmlProtocol).value;//Protocolo
  var camera = document.getElementById(htmlCamera).value;//Camara
  var t = document.getElementById(htmlTypeOfOption).value;//Tipo Entrada/Salida
  var ref_camera_id = id_cam;
  //--

  if(id_cam == 0 && sessionStorage.getItem(id) != null){
    id_cam = 0;  
    result = sessionStorage.getItem(id);
    aux = result.split(" | ");
    ref_camera_id = aux[1];
    $("#camera".concat(id)+ " option[value='"+ aux[0] +"']").prop('selected',true);
  }else{
    if(id_cam == 0){
      $.ajax({
        type: "GET",
        url: "http://"+localhost+"/cameras/1",
        success:
          function(result){
            $("#camera".concat(id)+ " option[value='"+result.ref_camera.reference  + " - " +result.brand.brand +"']").prop('selected',true);        
            ref_camera_id = result.ref_camera.id;
          }
      });
    }
  }

  //Funcion que utilizo para validar campos de entrada de texto html
  result = validate_form(description,user,password,ip,port,camera);  
  //--

  //Si la funcion vailidate_form regresa un true (TODO ESTA BIEN)
  if(result == true){
    //Verifica el estado de la camara (Enable/Disable)
    if(s.checked == true){
      s = true;
    }else{
      s = false;
    }
      
     $.ajax({
        type: "GET",
        url: "http://"+localhost+"/parking_cameras/"+id,
        success:
          function(result){
            $.ajax({
              type: "PUT",
              url: "http://"+localhost+"/parking_cameras/"+id,
              data: {
                  "parking_camera":{
                      "state": s,
                      "user": user,
                      "password": password,
                      "ip": ip,
                      "port":port,
                      "type_of": t,
                      "description":description,
                      "ref_camera":ref_camera_id,
                      "protocol": protocol
                  }
              },
              success:
                function(result){
                  location.reload();
                },
              error:
                function(result){
                }
            });
          },
        error:
          function(result){
            $.ajax({
              type: "POST",
              url: "http://"+localhost+"/parking_cameras/",
              data: {
                  "parking_camera":{
                      "state": s,
                      "user": user,
                      "password": password,
                      "ip": ip,
                      "port":port,
                      "type_of": t,
                      "description":description,
                      "ref_camera":ref_camera_id,
                      "id":id,
                      "protocol" :protocol
                  }
              },
              success:
                function(result){
                  var count_update = sessionStorage.getItem('count_cameras');
                  count_update++;
                  sessionStorage.removeItem('count_cameras');
                  sessionStorage.setItem('count_cameras',count_update);
                  location.reload();
                },
              error:
              function(result){
                }
            });
          }

      });
    
  }else{
    alert("Faltan o estan errados algunos datos ingresados");
  }
}

var validate_form = function(description, user, password, ip, port,camera){
  var validation = 0;

  if(description != "" && user != "" && password  != ""){
    validation += 1;
  }

  if(ip != "" && port != "" && camera != ""){
      var result = validate_ip(ip);
      if(result == true){
        validation +=1;
      }

      result = validate_port(port);
      if(result == true){
        validation +=1;
      }
  }

  if(validation == 3){
    return true;
  }else{
    return false;
  }
}

var validate_ip = function(ip){
  
  var patronIp = new RegExp("^([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})$");
  
  if(patronIp.test(ip)==true){
    var valores = ip.split(".");

    if((valores[0]<=255) && (valores[1]<=255) && (valores[2]<=255) && (valores[3]<=255)){
      return true;  
    }else{
      return false;
    }
  }else{
    alert("Ingrese datos correctos");
    return false;
  }
}

var validate_port = function(port){
  if(/^([0-9])*$/.test(port)==true){
    if(port > 0){
      return true;
    }else{
      return false;
    }
  }
}

$(document).ready(function()
{

  url = window.location.href;

  var ip = function(url){
    var n = url.substring(6,url.length); 
    n = n.split("/");   
    return n[1];
  }

  localhost = ip(url);
  var get_localhost =  function(){
    $.ajax({
        type: "GET",
        url: "http://"+localhost+"/get_conf_manager",
        success:
          function(result){
            localhost = result.ip;
            localhost+=":3000";
            alert(localhost);
          }
    });
  }

  var cleanInput = function(){

    var count = sessionStorage.getItem('count_cameras');

    $.ajax({
        type: "GET",
        url: "http://"+localhost+"/settings/1",
        success:
          function(result){
            if(result.number_of_cameras > count){
              for (var i = count; i <= result.number_of_cameras; i++) {
                $("#user".concat(i)).val("");
                $("#description".concat(i)).val("");
                $("#ip".concat(i)).val("");
                $("#port".concat(i)).val("");
                $("#password".concat(i)).val("");
              }
            }
          }
    });
  }

  var getConf = function(){

    $.ajax({
      type: "GET",
      url: "http://"+localhost+"/parking_cameras",
      success: 
        function(result){
          //Funcion para cargar los datos
          getData(result.count);
          sessionStorage.setItem('count_cameras',result.count);
        }
    });
  };

  var getData = function(count){

    for (var i = 1; i <= count; i++) {

      $.ajax({
        type: "GET",
        url: "http://"+localhost+"/parking_cameras/"+i,
        success:
          function(result){
            $("#description".concat(result.id)).val(result.description);
            $("#user".concat(result.id)).val(result.user);
            $("#password".concat(result.id)).val(result.password);
            $("#ip".concat(result.id)).val(result.ip);
            $("#port".concat(result.id)).val(result.port);

            //Funcion para obtener la marca
            getBrand(result.ref_camera_id,result.id);

            if(result.protocol == "RTSP"){
              $("#protocol".concat(result.id)+ " option[value='RTSP']").prop('selected',true);
            }

             if(result.type_of == "Salida"){
                $("#type_of_option".concat(result.id)+" option[value='Salida']").prop('selected',true);
            }

            if(result.state == true){
              $("#state".concat(result.id)).prop("checked",true);
            }else{
              $("#state".concat(result.id)).prop("checked",false);
            }
          }
      });
    }
  }

  var getBrand = function(ref_camera_id,id){

    $.ajax({
      type: "GET",
      url: "http://"+localhost+"/cameras/"+ref_camera_id,
      success: 
        function(result){
          $("#camera".concat(id)+ " option[value='"+result.brand.brand +" - " +result.ref_camera.reference +"']").prop('selected',true);
            
          var brand = result.brand.brand + " - " + result.ref_camera.reference + " | " +result.ref_camera.id;
            
          sessionStorage.setItem(id.toString(),brand);
        },
      error:
        function(result){

        }
    });
  }

  //get_localhost();
  getConf();
  cleanInput();
  
  $(document).on('ajax:success', '#edit_parking_setting_1', function(event, data){
    location.reload();
  });
  
  $(document).on('ajax:error', '#edit_parking_setting_1', function(event, data){
    alert(data + " Error!");
  });

  $('#configurar input:submit').on('click', function(){
  	alert("URLs configuradas.");
  	$('#configurar').foundation('reveal', 'close');  	
    	return true;
  });

  $("#type_of_option").change(function(){
    type_of = $("#type_of_option").val();
  });


  $("#edit").click(function(){

    url = "http://"+localhost+"/settings/1";
    $.ajax({
      type: "GET",
      url: url,
      success:
        function(result){
          $("#name").val(result.name);
          $("#owner").val(result.owner);
          $("#observations").val(result.observations);
          $("#maximum_capacity").val(result.maximum_capacity);
          $("#number_of_entries").val(result.number_of_entries);
          $("#location").val(result.location);
          $("#number_of_cameras").val(result.number_of_cameras);//Solucionar
        },
      error:
        function(result){
          alert(result);
        }
    });
    $('#edit_modal').foundation('reveal', 'open');
  });
  
  var number_aux = 0;

  $("#update").click(function(){
      
      var url = "http://"+localhost+"/settings/1";
      var name = $("#name").val();
      var owner = $("#owner").val();
      var observations = $("#observations").val();
      var maximum_capacity = $("#maximum_capacity").val();
      var number_of_entries = $("#number_of_entries").val();
      var location = $("#location").val();
      var number_of_cameras = $("#number_of_cameras").val();
      
      $.ajax({
        type: "GET",
        url: "http://"+localhost+"/settings/1",
        success:
          function(result){
            
            number_aux = result.number_of_cameras;
            
            if(number_of_cameras < number_aux){
            
              var difference =  parseInt(number_of_cameras) + 1;
              var count = sessionStorage.getItem('count_cameras');
              
              for (var i = difference; i <= count; i++) {

                var error = 0;
                $.ajax({
                  type: "DELETE",
                  url: "http://"+localhost+"/parking_cameras/"+i,
                  success:
                    function(result){
                    },
                  error:
                    function(result){
                      error = 1;
                      i -= 1;
                      $.ajax({
                          type: "DELETE",
                          url: "http://"+localhost+"/parking_cameras/"+i,
                          success:
                            function(result){
                            }
                      });
                    }                         
                });

                if(error == 1){
                  break;
                }
              }//Fin_for
            }
          }
      });

      $.ajax({
          type: "PUT",
          url: url,
          data:{
            "parking_setting":{
              "name": name,
              "owner": owner,
              "observations": observations,
              "maximum_capacity": maximum_capacity,
              "number_of_entries": number_of_entries,
              "location": location,
              "number_of_cameras" : number_of_cameras
            }
          },
          success:
            function(result){

          },
          error: 
            function(result){
            }
        });

      $("#edit_modal").foundation('reveal', 'close'); 
      setTimeout(function(){
         window.location.reload();    
      },1500);
     
  });

  $("#conf_manager").click(function(){
    
    $.ajax({
      type: "GET",
      url: "http://"+localhost+"/get_conf_manager",
      success:
        function(result){
          $("#ip_host").val(result.ip);
          $("#mask_host").val(result.mask);
          $("#gateway_host").val(result.gateway);
          $("#dns_uno_host").val(result.dns_one);
          $("#dns_dos_host").val(result.dns_two);

          $('#edit_manager').foundation('reveal', 'open');

        },
      error:
        function(result){
          alert("Error inesperado");
           $('#edit_manager').foundation('reveal', 'close');   
        }
    });

  });
  
  
  $("#ip_host").bind("keypress keyup focusout",function(){
    event($("#ip_host"));
  });

  $("#mask_host").bind("keypress keyup focusout",function(){
    event($("#mask_host"));
  });

  $("#gateway_host").bind("keypress keyup focusout",function(){
    event($("#gateway_host"));
  });

  $("#dns_uno_host").bind("keypress keyup focusout",function(){
    event($("#dns_uno_host"));
  });

  $("#dns_dos_host").bind("keypress keyup focusout",function(){
    event($("#dns_dos_host"));
  });

  $("#send_conf").click(function(){
      var r = confirm("¿Esta seguro de reailizar cambios en la red?");
      if(r){
        var ip = $("#ip_host").val();
        var mask = $("#mask_host").val();
        var gateway = $("#gateway_host").val();
        var dns1 = $("#dns_uno_host").val();
        var dns2 = $("#dns_dos_host").val();
        var result = validate_conf_manager(ip,mask,gateway,dns1,dns2);
        console.log(result);
        if(result){
          $.ajax({
            type: "POST",
            url: "http://"+localhost+"/conf_manager",
            data: {
                "conf_manager":{
                  ip: ip,
                  mask: mask,
                  gateway: gateway,
                  dns_one: dns1,
                  dns_two: dns2
                }
            },
            success:
              function(result){
                console.log(result);
                  reboot_System();
                  $('#edit_manager').foundation('reveal', 'close');
                  
                  alert("El equipo se reiniciara en menos de 2 minutos\n Y sera redireccionado a: "+ip
                    +"\nPor favor espere...");

                  setTimeout(function(){
                     location.assign("http://"+ip);
                  },70000); 
              },
            error:
              function(result){
                error = false;
                alert("Error!");
              }
          });
        }else{
            $('#edit_manager').foundation('reveal', 'close');
          alert("Alguno datos estan errados");
        }
      }else{
        $('#edit_manager').foundation('reveal', 'close');
        alert("openracion cancelada");
      }
  });
  
  var event = function(element){
    var result = validate_ip_no_message(element.val());
    if (result) {
      box_shadow(element,true);
    }else{
      box_shadow(element,false);
    }
  }

  var validate_ip_no_message = function(ip){
    
    var patronIp = new RegExp("^([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3}).([0-9]{1,3})$");
    if(patronIp.test(ip)==true){
      var valores = ip.split(".");
      if((valores[0]<=255) && (valores[1]<=255) && (valores[2]<=255) && (valores[3]<=255)){
        return true;  
      }else{
        return false;
      }
    }else{
      return false;
    }
  }


  var validate_conf_manager =  function(ip,mask,gateway,dns1,dns2){
    var r =  validate_ip_no_message(ip);
    var r2 = validate_ip_no_message(mask);
    var r3 = validate_ip_no_message(gateway);
    var r4 = validate_ip_no_message(dns1);
    var r5 = validate_ip_no_message(dns2);

    if(r != false && r2 != false && r3 != false && r4 != false && r5 != false){
      return true;
    }else{
      return false;
    }
  }

    //Funcion para agrgar  boxshadow
    var box_shadow = function(element,status){
      if(status==true){
        element.css("box-shadow","0.02em 0.02em 0.02em 0.02em #01DF01");
        element.css("border-color","#01DF01");
      }else{
        element.css("box-shadow","0.02em 0.02em 0.02em 0.02em #FF0000");
        element.css("border-color","#FF0000");
      }
    }

   //Funcion que carga las configuraciones de red
    var getConfManager = function(){
      $.ajax({
        type: "GET",
        url: "http://"+localhost+"/get_conf_manager",
        success:
          function(result){
            $("#ip_host").val(result.ip);
            $("#mask_host").val(result.mask);
            $("#gateway_host").val(result.gateway);
            $("#dns_uno_host").val(result.dns_one);
            $("#dns_dos_host").val(result.dns_two);
          },
        complete:
           function(result){
               box_shadow($("#ip_host"),validate_ip_no_message($("#ip_host").val()));
               box_shadow($("#mask_host",validate_ip_no_message($("#mask_host").val())));
               box_shadow($("#gateway_host",validate_ip_no_message($("#gateway_host").val())));
               box_shadow($("#dns_uno_host",validate_ip_no_message($("#dns_uno_host").val())));
               box_shadow($("#dns_dos_host",validate_ip_no_message($("#dns_dos_host").val())));
           }
      });
   }
   getConfManager();

   $("#backup_usb").click(function(){
      $('#backup_usb_modal').foundation('reveal', 'open');
   });

    var treatmentString = function(myString){
      myString = myString.replace("disk","");
      myString = myString.replace("part","");
      myString = myString.replace("|","");
      myString = myString.replace("`","");
      myString = myString.replace("-","");
      myString = myString.replace("\n","");
      myString = myString.split("0");
      myString = myString[1];
      return myString;
    }
    
    $("#refresh").click(function(){
      $("#device").empty(); //elimina el contenido..
      $.ajax({
        type: "GET",
        url: "http://"+localhost+"/path_result",
        success:
          function(result){
            for(var i = 1; i < result.settings.length; i++){
              var data = result.settings[i]; 
              if( data.search("disk") == -1 && 
                data.search("mmcblk0p1") == -1 &&  data.search("mmcblk0p2") == -1 ) {
                path = treatmentString(data);
                name_device = path.split("/");
                if(name_device != "  " && name_device.length >= 4){
                  console.log(name_device);
                  $("#device").append( $("<option></option>").attr( "value",name_device[3]).text( name_device[3]));//append
                }
              }//Fin Si
            }//Fin Para
          }, //Fin Succsess
        error:
          function(result){
             $("#device").val(result.device);
          }
      });//Fin Ajax
    });//Fin Funcion

   $("#save_backup").click(function(){
      var device = $("#device").val();
      $.ajax({
        type: "POST",
        data: {"device":device+"/"},
        url: "http://"+localhost+"/backup_usb",
        success: 
          function(result){ 
            if(result.response == true && response_cp == true){
              alert("Exito!");
            }else if(result.response == false){
              alert("Error!");
            }else{
              alert(result.response);
            }
        },
        error:
          function(result){
            alert("Error!");
          }
      });
      $('#backup_usb_modal').foundation('reveal', 'close');
   });

  $("#drop_tables").click(function(){  
    
    var rc = confirm("¿Eliminar todo los datos?"+
      "\n!Estos son de los vehiculos que han ingresado y salido!.");
    if(rc){
      rc = confirm("¿Esta seguro de continuar con la operación?");
      if(rc){
        $.ajax({
          type: "GET",
          url: "http://"+localhost+"/drop_tables",
          success:
            function(result){
              console.log("Drop tables");
            }
          });  
      }else{
        alert("Operacion cancelada");
      }
    }else{
      alert("Operacion cancelada");
    }
    
  });

  var reboot_System = function(){
    $.ajax({
      url: "http://"+localhost+"/reboot_system",
      type: "GET",
      success:
        function(result){
          console.log(result);
        }
    })
  }
});