$(document).ready(function(){
	$(document).on('click', '.edit_plate', function(event){
		var entry_id = $(this).attr('id').split('_')[2];
		var temp = url_formatted() + '/entries/'+entry_id+'.json';
		$('#edit_plate_form').attr('action',temp);
		$('#edit_plate_segment_url').attr('src', $('#entry_plate_segment_url_'+entry_id).attr('src'));
		$('#entry_plate_chars').val($('#entry_plate_char_'+entry_id).html());
	});
	
	$(document).on('click', '.paring_plate', function(event){
		var entry_id = $(this).attr('id').split('_')[2];
		var temp = url_formatted() + '/matchings/'+entry_id+'.json';
		$('#matching_plate_form').attr('action',temp);
		temp = url_formatted() + '/matchings/'+entry_id;

		$.get( temp, function( data ) {
		  $("#matching_plate_chars tbody" ).html(data);
		});
		$('#matching_plate_segment_url').attr('src', $('#entry_plate_segment_url_'+entry_id).attr('src'));
		$('#matching_plate_chars').html($('#entry_plate_char_'+entry_id).html());
	});
	
	$(document).on('ajax:success', '#edit_plate_form', function(event, data){
    location.reload();
  });
  
  $(document).on('ajax:error', '#edit_plate_form', function(event, data){
    alert(data);
    console.log(data);
  });

  $(document).on('ajax:success', '#matching_plate_form', function(event, data){
    location.reload();
  });
  
  $(document).on('ajax:error', '#matching_plate_form', function(event, data){
    alert(data);
    console.log(data);
  });

  $('#entry_plate_chars').keyup(function(){
		this.value = this.value.toUpperCase();
	});
});