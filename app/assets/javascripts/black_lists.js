$(document).ready(function(){

  $('#new_black_list').submit(function(){
    var plate = $('#black_list_plate').val();
    var go = "false";

    if(plate.match(/^\s*$/))
    {
      $('#nueva_placa_mensaje').html('- Placa no puede estar vacía.');
      return false;
    }else{
      if(!plate.match(/^[A-Z]{3}[0-9]{3}$/))
      {
        $('#nueva_placa_mensaje').html('- Placa inválida.');
        return false; 
      }else{
        $.ajax({
          async: false,
          type: "POST",
          url: "/black_lists/search_w",
          data: {id: plate},
          success: function(json){
            var response = $.parseJSON(json);
            //alert(response);
            if(response=="1"){
              var conf = confirm("ATENCIÓN: esta placa se encuentra en Lista Blanca. ¿Desea cambiarla a Lista Negra?");
              if(conf==true)
              {
                $('#black_list_search_w').val('true');
                go = "true";
              }
            }
            else
            {
              $('#black_list_search_w').val('false');
              go = "true";
            }
          }
        });         
      }
    }
    
    if(go=="true")
    {
      return true;
    }else{
      return false;
    }
  });

	$(document).on('ajax:success', '#new_black_list', function(event, data){
    $('#black_list_plate').val('');
    $('#black_list_motivo').val('');
    location.reload();
  });
  
  $(document).on('ajax:error', '#new_black_list', function(event, data){
    alert(data.responseText);
    console.log(data);
  });

  $('#black_list_plate').on('focus', function(){
    $('#nueva_placa_mensaje').html('');
    $('#black_list_plate').val('');
  });

  $('#agregar_placa_negra').on('click', function(){
    $('#nueva_placa_mensaje').html('');
    $('#black_list_plate').val('');
  });

  $('#continue').click(function(){
    $(".black_lists:checked").each(function(){
      var id = $(this).val();
      $.ajax({
        async: false,
        type: "POST",
        url: "/black_lists/destroy",
        data: {id: id},
        success: function(json){
        }
      });  
    });
    location.reload();
  });

  /*$('#continue').click(function(){
    $(".black_lists:checked").each(function(){
      $.ajax({
        url: url_formatted()+"/black_lists/"+$(this).val()+".json", 
        type:"DELETE"
      });  
    });
    document.location.reload(true);
    //location.reload(true);
  });*/

  $('#agregar_placa_negra').click(function(){
    $('#black_list_plate').val('');
    $('#black_list_motivo').val('');
  });
  
  $('#black_list_plate').keyup(function(){
    this.value = this.value.toUpperCase();
  });
  
});