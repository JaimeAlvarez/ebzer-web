var localhost = "";
$(document).ready(function(){

	url = window.location.href;

  	var ip = function(url){
    	var n = url.substring(6,url.length); 
    	n = n.split("/");   
    	return n[1];
  	}

 	localhost = ip(url);

	var id_cam;
	var getId =function (id){
		id_cam = id;
	}

	$("#protocol").change(function(){
		protocol = $("#protocol").val();
	});

	$("#type_of").change(function(){
		type_of = $("#type_of").val();
	});

	$("#reference_text").bind("keypress keyup",function(){
		$("#reference_text").val(functionToUpperCase($("#reference_text").val()));
	});

	$("#brand_text").bind("keypress keyup",function(){
		$("#brand_text").val(functionToUpperCase($("#brand_text").val()));
	});

	$("#brand").bind("keypress keyup",function(){
		$("#brand").val(functionToUpperCase($("#brand").val()));
	});
	
	$("#reference").bind("keypress keyup",function(){
		$("#reference").val(functionToUpperCase($("#reference").val()));
	});

	var functionToUpperCase = function(element){
		element = element.toUpperCase();
		return element;
	}

	$("#send").click(function(){
		
		var url = "http://"+localhost+"/cameras" ;
		var brand = $("#brand").val();
		var reference = $("#reference").val();
		var path = $("#path").val();

		 if(brand == null || reference == null || path == null || brand == "" || reference == "" || path == ""){
		 	alert("error");
		 }else{
			$.ajax({
				type: "POST",
				url: url,
				data:{
					"refcam":{
						"brand" :brand,
						"reference" : reference,
						"path" : path
					}
				},
				success: 
					function(result){
						location.reload();
						window.location.hash='';
						location.href = "http://"+localhost+"/settings.html"
				},
				error:
					function(result){
						console.log(result);
				}
			});
		}
	});

});

$("#send_and_update").click(function(){
//Boton Guardar de Camera/index

	var url = "http://"+localhost+"/cameras/" 
	var reference = $("#reference_text").val();
	var brand = $("#brand_text").val();
	var id = $("#id_camera").val();
	var path = $("#path_text").val();

	$.ajax({
		type: "PUT",
		url: url + id,
		data:{
			"refcam":{
				"reference": reference,
				"brand": brand,
				"path": path
			}
		},
		success:
			function(result){
				console.log(result);
				location.reload();
		},
		error: 
			function(result){
				console.log(result);
			}
	});
	
});

var getCamera = function(id){
	$.ajax({
		type: "GET",
		url: "http://"+localhost+"/cameras/"+ id,
		success:
			function(result){

				$("#id_camera").val(id);
				$("#brand_text").val(result.brand.brand);
				$("#reference_text").val(result.ref_camera.reference);
				$("#path_text").val(result.ref_camera.path)

				$('#edit').foundation('reveal','open');
			},
		error:
			function(result){
				console.log(result)
			}

	});
}