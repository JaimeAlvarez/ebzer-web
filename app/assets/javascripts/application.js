// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
// require_tree 
//= require responsive-tables
//= require websocket_rails/main
//var app_name = '192.168.1.102';
var app_name = '127.0.0.1';
var sub_uri = '';
var app_port = '';
//var ws_port = '3001';
var ws_port = '3001';



function url_formatted(){
	if (sub_uri == '' && app_port == ''){
		return '';
  }else{
  	if (sub_uri != '' && app_port != ''){
  		return '/'+sub_uri + ':' + app_port;
  	}else{
  		if (sub_uri != ''){
  			return '/'+sub_uri;
  		}
  	}
  }
}

function url_formatted_ws(){
	if (app_name == '' && ws_port == ''){
		return '';
  }else{
  	if (app_name != '' && ws_port != ''){
  		return app_name + ':' + ws_port;
  	}else{
  		if (app_name != ''){
  			return app_name;
  		}
  	}
  }
}
$(function(){ $(document).foundation(); });
