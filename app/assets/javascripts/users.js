// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).ready(function(){
	$('#change_password form').attr('id', 'edit_password');
	$('#change_password form').attr('class', 'edit_password');

	//Acciones de validación para registro de usuario nuevo.
	$('#new_user').on('submit', function(){
		$('#errores').empty();
		var fields = [];
		var e = false;
		$('#new_user input.input_new_form').each(function(index){
			if(checkEmpty($(this).val()))
			{
				e = true;
				fields.push(checkIndex(index));
			}			
		});

		if(fields.length > 0){			
			if(fields.length > 1){
				$('#errores').append('<p>- Los siguientes campos no pueden estar vacíos: '+fields+'.</p>');
			}

			if(fields.length == 1){
				$('#errores').append('<p>- El siguiente campo no puede estar vacío: '+fields+'.</p>');
			}
		}else{		
			if(!checkSizeOfPassword($('#user_password').val())){
				e = true;
				$('#errores').append('<p>- Contraseña debe ser mayor o igual a seis caracteres. </p>');
			}

			if(!checkSizeOfColombianId($('#user_colombian_id').val()))
			{
				e = true;
				$('#errores').append('<p>- Cédula debe ser mayor o igual a siete caracteres. </p>');	
			}

			if(!checkColombianId($('#user_colombian_id').val()))
			{
				e = true;
				$('#errores').append('<p>- Cédula no debe tener letras. </p>');
			}
			if(!checkPasswords($('#user_password').val(), $('#user_password_confirmation').val())){
				e = true;
				$('#errores').append('<p>- Contraseña y confirmación no coinciden. </p>');
			}
		}

		
		if(e)
		{
			alert("Hay errores en el formulario.");
			var body = $("html, body");
			body.stop().animate({scrollTop:0}, '500', 'swing');
			return false;
		}else
		{
			return true;
		}
	});

	//Acciones de validación para edición de usuario.
	$('.edit_user').on('submit', function(){
		$('#errores').empty();
		var fields = [];
		var e = false;
		$('.edit_user input.input_edit_form').each(function(index){
			if(checkEmpty($(this).val()))
			{
				e = true;
				fields.push(checkIndex(index));
			}			
		});

		if(fields.length > 0){			
			if(fields.length > 1){
				$('#errores').append('<p>- Los siguientes campos no pueden estar vacíos: '+fields+'.</p>');
			}

			if(fields.length == 1){
				$('#errores').append('<p>- El siguiente campo no puede estar vacío: '+fields+'.</p>');
			}
		}else{		

			if(!checkSizeOfColombianId($('#user_colombian_id').val()))
			{
				e = true;
				$('#errores').append('<p>- Cédula debe ser mayor o igual a siete caracteres. </p>');	
			}

			if(!checkColombianId($('#user_colombian_id').val()))
			{
				e = true;
				$('#errores').append('<p>- Cédula no debe tener letras. </p>');
			}
			if(!checkPasswords($('#user_password').val(), $('#user_password_confirmation').val())){
				e = true;
				$('#errores').append('<p>- Contraseña y confirmación no coinciden. </p>');
			}
		}

		
		if(e)
		{
			alert("Hay errores en el formulario.");
			var body = $("html, body");
			body.stop().animate({scrollTop:0}, '500', 'swing');
			return false;
		}else
		{
			return true;
		}
	});

	//Acciones de validación para cambio de contraseña.
	$('#edit_password').on('submit', function(){
		$('#errores').empty();
		var fields = [];
		var e = false;
		$('#edit_password input.input_edit_form').each(function(index){
			if(checkEmpty($(this).val()))
			{
				e = true;
				fields.push(checkIndex(index+3));
			}			
		});

		if(fields.length > 0){			
			if(fields.length > 1){
				$('#errores').append('<p>- Los siguientes campos no pueden estar vacíos: '+fields+'.</p>');
			}

			if(fields.length == 1){
				$('#errores').append('<p>- El siguiente campo no puede estar vacío: '+fields+'.</p>');
			}
		}else{

			if(!checkSizeOfPassword($('#user_password').val())){
				e = true;
				$('#errores').append('<p>- Contraseña debe ser mayor o igual a seis caracteres. </p>');
			}		

			if(!checkPasswords($('#user_password').val(), $('#user_password_confirmation').val())){
				e = true;
				$('#errores').append('<p>- Contraseña y confirmación no coinciden. </p>');
			}
		}

		
		if(e)
		{
			var body = $("html, body");
			body.stop().animate({scrollTop:0}, '500', 'swing');
			return false;
		}else
		{
			return true;
		}
	});
	

	function checkEmpty(value){
		if(value.match(/^\s*$/))
		{
			return true;
		}else{
			return false;
		}
	}

	function checkSizeOfPassword(value){
		if (value.length >= 6) {
			return true;
		} else{
			return false;
		}
	}

	function checkSizeOfColombianId(value){
		if (value.length >= 7) {
			return true;
		} else{
			return false;
		};
	}

	function checkColombianId(value){
		if (/^\d+$/.test(value)) {
			return true;
		} else{
			return false;
		};
	}

	function checkPasswords(val1, val2){
		if(val1 == val2)
		{
			return true;
		}else{
			return false;
		}
	}

	function checkIndex(value){
		switch(value){
			case 0:
				return "nombres y apellidos";
				break;
			case 1:
				return " cédula";
				break;
			case 2:
				return " e-mail";
				break;
			case 3:
				return " contraseña";
				break;
			case 4:
				return " confirmación de contraseña";
				break;
		}
	}
});