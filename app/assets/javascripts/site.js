$(document).ready(function(){

	var arrayVideo_in = new Array();
	var arrayVideo_out = new Array();
	var dispatcher = new WebSocketRails(url_formatted_ws()+'/websocket');
	var channel = dispatcher.subscribe('real_broadcast');
	var opened = false;
	var opened_1  = false;
	var opened_2  = false;
	var editarplaca_opened = false;
	var white_list_opened = false;
	var black_list_opened = false;
	var index;
	var value;
	var vlc;
	var url;
	var parking_cameras_ID = "";
	var idPC = "";
	var localhost = "";
	url = window.location.href;

	 var ip = function(url){
	    var n = url.substring(6,url.length);
	    n = n.split("/");
	    return n[1];
 	 }	
	
	localhost = ip(url);
	
	if(sessionStorage.getItem('value_in') == null){
		sessionStorage.setItem('value_in',0);
		sessionStorage.setItem('arrayVideo_in',0);
	}

	if(sessionStorage.getItem('value_out') == null){
		sessionStorage.setItem('arrayVideo_out',0);
		sessionStorage.setItem('value_out',0);
	}

	channel.bind('other_entries', function(data) {
		if(data.entry.entry_type == 1){
		/*ENTRADA*/
			if(data.entry.plate_segment_url == ""){
				$('#entry_full_image_link').attr('href','http://'+data.entry.full_image_url).html('Entrada manual.');
			}
			else
			{
				$('#entry_full_image_link').attr('href','http://'+data.entry.full_image_url).html('<img id="entry_plate_segment" src="http://'+data.entry.plate_segment_url+'" style="border: 1px solid blue;">');
			}
			$('#entry_plate').html(data.entry.plate_chars);
			$('#entry_date').html(data.entry.fecha);
			$('#entry_hour').html(data.entry.hora);
			$('#estado_entrada').html('Acceso denegado.').css("color", "red");
			//alert('Entrada');
		}else{
			
			if(data.entry.entry_type == 2){
			/*SALIDA*/
			if(data.entry.plate_segment_url == "")
			{
				$('#exit_full_image_link').attr('href','http://'+data.entry.full_image_url).html('Salida manual.');
			}
			else
			{
				$('#exit_full_image_link').attr('href','http://'+data.entry.full_image_url).html('<img id="exit_plate_segment" src="http://'+data.entry.plate_segment_url+'" style="border: 1px solid blue;">');	
			}		  	
			
			$('#exit_plate').html(data.entry.plate_chars);
			$('#exit_date').html(data.entry.fecha);
			$('#exit_hour').html(data.entry.hora);				
			$('#estado_salida').html('Acceso denegado.').css("color", "red");
			//alert('Salida');
			}
		}
		
		var entrance = data.entry.nro_entradas;
		var available = data.entry.capacidad_disponible;

		$('#small_entrance').html(entrance);
		$('#small_available').html(available);
		$('#medium_entrance').html(entrance);
		$('#medium_available').html(available);
	  	$('#large_entrance').html(entrance);
	  	$('#large_available').html(available);
	  
		if(data.entry.necesita_editar == true){
		  	var temp = "<tr id='entry_"+data.entry.id+"'><td>"+data.entry.entry_type_str+"</td><td id='entry_plate_char_"+data.entry.id+"'>";
		  	temp = temp +data.entry.plate_chars+"</td><td>"+data.entry.fecha;
		  	temp = temp + "</td><td>"+data.entry.hora;
		  	temp = temp + "</td><td><ul class='clearing-thumbs' data-clearing><li><a href='http://";
		  	temp = temp + data.entry.full_image_url+"'><img id='entry_plate_segment_url_"+data.entry.id+"' class='imgsegment' src='http://";
		  	temp = temp + data.entry.plate_segment_url;
		  	temp = temp + "'></a></li></ul></td><td><i id='edit_entry_"+data.entry.id+"' data-reveal-id='editarplaca' class='fa fa-pencil fa-2x edit_plate'></i></td></tr>";
			$('#rows_correct_plate_chars tbody').prepend(temp);
		  	
		  	if(opened_1 == false || opened_2 == false){
				if(opened == false){
					opened = true;
					editarplaca_opened = true;
										
					var temp = url_formatted() + '/entries/'+data.entry.id+'.json';
					$('#edit_plate_form').attr('action',temp);
					$('#edit_plate_segment_url').attr('src', 'http://'+data.entry.plate_segment_url);
					$('#entry_plate_chars').val(data.entry.plate_chars);			
					$('#editarplaca').foundation('reveal', 'open');
				}
			}
		  	//console.log('Necesita Editarse!!!');
	    }

	  console.log(data);
	  //alert(data.plate_chars);
	});

	channel.bind('black_list', function(data) {
  		if(opened == true){
  			opened = false;
			$('#editarplaca').foundation('reveal', 'close');
		}

		$('#black_list_plate_segment_url').attr('src', 'http://'+data.entry.plate_segment_url);
		$('#black_list_plate_chars').html("<h2>"+data.entry.plate_chars+"</h2>");
		$('#black_list_motivo').html("<h2>Motivo: "+data.entry.motivo_black_list+"</h2>");

		if(opened_2 == true){
			opened_2 = false;
			$('#white_list_plate').foundation('reveal', 'close');
		}
		if(opened_1 == false){
			opened_1 = true;
			$('#black_list_plate').foundation('reveal', 'open');	
		}
		if(data.entry.entry_type == 1){
		  	/*ENTRADA*/
		  	if(data.entry.plate_segment_url == ""){
		  		$('#entry_full_image_link').attr('href','http://'+data.entry.full_image_url).html('Entrada manual.');
		  	}
		  	else
		  	{
			  	$('#entry_full_image_link').attr('href','http://'+data.entry.full_image_url).html('<img id="entry_plate_segment" src="http://'+data.entry.plate_segment_url+'" style="border: 1px solid blue;" >');
		  	}
		  	$('#entry_plate').html(data.entry.plate_chars);
		  	$('#entry_date').html(data.entry.fecha);
		  	$('#entry_hour').html(data.entry.hora);
		  	$('#estado_entrada').html('Acceso denegado.').css("color", "red");
		}else{
		  	if(data.entry.entry_type == 2){
		  		/*SALIDA*/
		  		if(data.entry.plate_segment_url == "")
		  		{
		  			$('#exit_full_image_link').attr('href','http://'+data.entry.full_image_url).html('Salida manual.');
		  		}
		  		else
		  		{
		  			$('#exit_full_image_link').attr('href','http://'+data.entry.full_image_url).html('<img id="exit_plate_segment" src="http://'+data.entry.plate_segment_url+'" style="border: 1px solid blue;" >');	
		  		}
			  	$('#exit_plate').html(data.entry.plate_chars);
			  	$('#exit_date').html(data.entry.fecha);
			  	$('#exit_hour').html(data.entry.hora);				
			  	$('#estado_salida').html('Acceso denegado.').css("color", "red");
		  	}
		}
		console.log(data);
	});

	channel.bind('white_list', function(data) {
		if(editarplaca_opened == true)
		{
			editarplaca_opened = false;
			$('#editarplaca').foundation('reveal', 'close');
		}
		if(black_list_opened == true)
		{
			black_list_opened = false;
			$('#black_list_plate').foundation('reveal', 'close');
		}

		if(data.entry.entry_type == 1){
		  	/*ENTRADA*/
		  	if(data.entry.plate_segment_url == ""){
		  		$('#entry_full_image_link').attr('href','http://'+data.entry.full_image_url).html('Entrada manual.');
		  	}
		  	else
		  	{
			  	$('#entry_full_image_link').attr('href','http://'+data.entry.full_image_url).html('<img id="entry_plate_segment" src="http://'+data.entry.plate_segment_url+'" style="border: 1px solid blue;" >');
		  	}
		  	$('#entry_plate').html(data.entry.plate_chars);
		  	$('#entry_date').html(data.entry.fecha);
		  	$('#entry_hour').html(data.entry.hora);
		  	$('#estado_entrada').html('Acceso permitido.').css("color", "green");
	  	}else{
		  	if(data.entry.entry_type == 2){
		  		/*SALIDA*/
		  		if(data.entry.plate_segment_url == "")
		  		{
		  			$('#exit_full_image_link').attr('href','http://'+data.entry.full_image_url).html('Salida manual.');
		  		}
		  		else
		  		{
		  			$('#exit_full_image_link').attr('href','http://'+data.entry.full_image_url).html('<img id="exit_plate_segment" src="http://'+data.entry.plate_segment_url+'" style="border: 1px solid blue;" > ');	
		  		}
			  	$('#exit_plate').html(data.entry.plate_chars);
			  	$('#exit_date').html(data.entry.fecha);
			  	$('#exit_hour').html(data.entry.hora);				
			  	$('#estado_salida').html('Acceso permitido.').css("color", "green");
		  	}
		}
		console.log(data);
	});

	$(document).on('click', '.edit_plate', function(event){
		opened = true;
		var entry_id = $(this).attr('id').split('_')[2];
		var temp = url_formatted() + '/entries/'+entry_id+'.json';
		$('#edit_plate_form').attr('action',temp);
		$('#edit_plate_segment_url').attr('src', $('#entry_plate_segment_url_'+entry_id).attr('src'));
		$('#entry_plate_chars').val($('#entry_plate_char_'+entry_id).html());
	});

	$(document).on('ajax:success', '#edit_plate_form', function(event, data){
    	$('#entry_'+data.entry.id).html('');
    	$('#editarplaca').foundation('reveal', 'close');
    	opened = false;
    	//console.log(data);
  	});

  	$(document).on('ajax:error', '#edit_plate_form', function(event, data){
    	alert(data);
    	console.log(data);
  	});

  	$(document).on('click', '#close_editar_placa', function(event, data){
  		opened = false;
  	});

  	$(document).on('click', '#continue', function(event, data){
  		$('#edit_plate_form').submit();
  	});  	

  	$('#entry_plate_chars').bind('keypress keydown keyup', function(e){
  		if(e.keyCode == 13){ 
    		e.preventDefault();
    		$('#confirmacion').foundation('reveal', 'open');
    	}
  	});

	$("#entrada_manual_button").click(function() {		
		//$('#entrada_manual img#entry_full_image_url').attr('src','http://192.168.1.65/cgi-bin/snapshot.cgi?chn=1' + new Date().getTime());
		$('#entrada_manual_plate').val('');
		$('#entrada_mensaje').html('');
	});

	$("#salida_manual_button").click(function() {
	  	//$('#salida_manual img#entry_full_image_url').attr('src','http://192.168.1.65/cgi-bin/snapshot.cgi?chn=1' + new Date().getTime());
		$('#salida_manual_plate').val('');
		$('#salida_mensaje').html('');
	});

	$("#entrada_manual form").submit(function () { 
		var plate = $('#entrada_manual_plate').val();         
        if(plate.match(/^\s*$/))
        {
        	$('#entrada_mensaje').html('- Placa no puede estar vacía.');
            return false;
        }else{
        	if(!plate.match(/[A-Z]{3}[0-9]{3}/))
		    {
		    	if(!plate.match(/[A-Z]{3}[0-9]{2}[A-Z]{1}/))
		        {
		        	$('#entrada_mensaje').html('- Placa inválida.');
		        	return false;
		        }else{
		          	return true;
		        }  
		    }else{
		    	return true;
			}
        }
	});

	$('#entrada_manual_plate').on('focus',function(){
		$('#entrada_mensaje').html('');
		$('#entrada_manual_plate').val('');
	});

	$('#salida_manual form').submit(function(){
		var plate = $('#salida_manual_plate').val();         
        if(plate.match(/^\s*$/))
        {
        	$('#salida_mensaje').html('- Placa no puede estar vacía.');
            return false;
        }else{
        	if(!plate.match(/[A-Z]{3}[0-9]{3}/))
		    {
		    	if(!plate.match(/[A-Z]{3}[0-9]{2}[A-Z]{1}/))
		        {
		        	$('#salida_mensaje').html('- Placa inválida.');
		        	return false;
		        }else{
		          	return true;
		        }  
		    }else{
		    	return true;
			}
        }
	});

	$('#salida_manual_plate').on('focus',function(){
		$('#salida_mensaje').html('');
		$('#salida_manual_plate').val('');
	});

  	$(document).on('ajax:success', '#entry_manual', function(event, data){
    	$('#entrada_manual').foundation('reveal', 'close');
    	$('#salida_manual').foundation('reveal', 'close');
    	opened = false;
    	//console.log(data);
  	});

  	$('#entry_plate_chars').keyup(function(){
		this.value = this.value.toUpperCase();
	});

	$('#entrada_manual_plate').keyup(function(){
		this.value = this.value.toUpperCase();
	});

	$('#salida_manual_plate').keyup(function(){
		this.value = this.value.toUpperCase();
	});

	$('#close_entrada_manual').click(function(){
		$('input#entrada_manual_plate').val("");
		$('#entrada_mensaje').html(' ');
	});

	$('#close_salida_manual').click(function(){
		$('input#salida_manual_plate').val("");
		$('#salida_mensaje').html(' ');
	});

	var getVideoIIn = function(){
		index = "";
		index = $("#select_in").val();
	
		$.ajax({
			type: "GET",
			url: "http://"+localhost+"/parking_cameras/"+index,
			success:
				function(result){
					
					if(result.port == "" && result.protocol == "RTSP"){
						url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+ "Streaming/Channels/2";
					}else{
						if(result.user == "" && result.password == "" && result.protocol == "HTTP"){
							url = result.protocol.toLowerCase() +  "://" +result.ip +":"+result.port+ "/video";					
						}else{
							url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+result.port+ "Streaming/Channels/2";					
						}
					}
					
				 	vlc = document.getElementById("embed_in");
					vlc.playlist.add(url,"live", ":network-caching=450");
					vlc.playlist.next();
					vlc.playlist.play();
				}
		});
		
	}

	var getVideoIOut = function(){
		index = "";
		index = $("#select_out").val();
		
		$.ajax({
			type: "GET",
			url: "http://"+localhost+"/parking_cameras/"+index,
			success:
				function(result){
					
					if(result.port == "" && result.protocol == "RTSP"){
						url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+ "Streaming/Channels/2";
					}else{
						if(result.user == "" && result.password == "" && result.protocol == "HTTP"){
							url = result.protocol.toLowerCase() +  "://" +result.ip +":"+result.port+ "/video";					
						}else{
							url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+result.port+ "Streaming/Channels/2";					
						}
					}
					
					$("#embed_out").attr("target", url);
					vlc = document.getElementById("embed_out");

					vlc.playlist.add(url);
					vlc.playlist.next();
					vlc.playlist.play();
				},
			error:
				function(result){

				}
		});
	}

	var ArrayGetData =  function(type_of){
		$.ajax({
			type: "GET",
			url: "http://"+localhost+"/parking_cameras/",
			success:
				function(result){
					if(type_of == "in"){
						parking_cameras_ID = result.id_parking_cameras_in;
						idPC = parking_cameras_ID.toString().split(",");

						for (var i = 0; i < idPC.length; i++) {
							ArrayPushData(idPC[i],type_of);
						}
					}else{
						parking_cameras_ID = result.id_parking_cameras_out;
						idPC = parking_cameras_ID.toString().split(",");

						for (var i = 0; i < idPC.length; i++) {
							ArrayPushData(idPC[i],type_of);
						}
					}
				},
			complete:
				function(){
					if(type_of == "in"){
						setTimeout(function(){
							setCameraSelected_in();
						},600);
					}else if(type_of == "out"){
						setTimeout(function(){
							setCameraSelected_out();
						},600);
					}
				}
		});
	}

	var ArrayPushData =  function(index,type_of){
		$.ajax({
			type: "GET",
			url: "http://"+localhost+"/parking_cameras/"+index,
			success:
				function(result){
					if(type_of == "in"){
						arrayVideo_in.push(result);
					}else if(type_of == "out"){
						arrayVideo_out.push(result);
					}
				}
		});
	}

	var setCameraSelected_in = function(){

		value = sessionStorage.getItem('value_in');
		$("#select_in option[value='"+value+"']").prop('selected',true);
		
		url = "";

		for(var i = 0; i < arrayVideo_in.length; i ++){
			var result = arrayVideo_in[i];
			
			if(result.id == value){

				if(result.protocol == "RTSP"){
					if (result.port == "") {
						url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+ "Streaming/Channels/2";
					}else{
						url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip + ":"+ result.port+ "/"+ "Streaming/Channels/2";
					}
				}else{
					if(result.user == "" && result.password == "" && result.protocol == "HTTP"){
						url = result.protocol.toLowerCase() +  "://" +result.ip +":"+result.port+ "/video";					
					}else{
						url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+result.port+ "Streaming/Channels/2";				
					}
				}

				$("#embed_in").attr("target", url);
				vlc = document.getElementById("embed_in");
				vlc.playlist.add(url);
				vlc.playlist.next();
				vlc.playlist.play();

				break;
			}
		}
	}

	var setCameraSelected_out = function(){
		
		value = sessionStorage.getItem('value_out');
		$("#select_out option[value='"+value+"']").prop('selected',true);
		
		url = "";
		
		for(var i = 0; i < arrayVideo_out.length; i ++){
			var result = arrayVideo_out[i];
			if(result.id == value){
	
				if(result.protocol == "RTSP"){
					if (result.port == "") {
						url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+ "Streaming/Channels/2";
					}else{
						url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip + ":"+ result.port+ "/"+ "Streaming/Channels/2";
					}
				}else{
					if(result.user == "" && result.password == "" && result.protocol == "HTTP"){
						url = result.protocol.toLowerCase() +  "://" +result.ip +":"+result.port+ "/video";					
					}else{
						url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+result.port+ "Streaming/Channels/2";					
					}
				}

				$("#embed_out").attr("target", url);
				vlc = document.getElementById("embed_out");

				vlc.playlist.add(url);
				vlc.playlist.next();
				vlc.playlist.play();

				break;
			}
		}
	}

	getVideoIIn(); //Carga el primer video de la camara de entrada
	getVideoIOut(); //Carga el primer video de la camara de salida
	ArrayGetData("in"); //Carga La informacion en los array de entrada
	ArrayGetData("out");// ""                 ""            de Salida	

	$("#select_in").change(function(){
		index = $("#select_in").prop('selectedIndex');
		value = $("#select_in").val();
		value = value.replace("[","");
		value = value.replace("]","");
		
		url = "";

		index+=1;
		var result = arrayVideo_in[index];
		
		if(result.port == "" && result.protocol == "RTSP"){
			url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+ "Streaming/Channels/2";
		}else{
			if(result.user == "" && result.password == "" && result.protocol == "HTTP"){
				url = result.protocol.toLowerCase() +  "://" +result.ip +":"+result.port+ "/video";					
			}else{
				url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+result.port+ "Streaming/Channels/2";					
			}
		}
		
		$("#embed_in").attr("target", url);
		vlc = document.getElementById("embed_in");

		vlc.playlist.add(url);
		vlc.playlist.next();
		vlc.playlist.play();
		getIndexIn(value);
	});

	$("#select_out").change(function(){
		index = $("#select_out").prop('selectedIndex');
		value = $("#select_out").val();
		value = value.replace("[","");
		value = value.replace("]","");
		
		url = "";
		
		index+=1;
		var result = arrayVideo_out[index];

		if(result.port == "" && result.protocol == "RTSP"){
			url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+ "Streaming/Channels/2";
		}else{
			if(result.user == "" && result.password == "" && result.protocol == "HTTP"){
				url = result.protocol.toLowerCase() +  "://" +result.ip +":"+result.port+ "/video";					
			}else{
				url = result.protocol.toLowerCase() +  "://" + result.user + ":" +result.password + "@" +result.ip +"/"+result.port+ "Streaming/Channels/2";					
			}
		}
		
		$("#embed_out").attr("target", url);
		vlc = document.getElementById("embed_out");

		vlc.playlist.add(url);
		vlc.playlist.next();
		vlc.playlist.play();
		getIndexOut(value);
	});

	var getIndexIn = function(value){
		if(sessionStorage.getItem('value_in') != null){
			sessionStorage.removeItem('value_in');
			sessionStorage.setItem('value_in',value);
		}
	}

	var getIndexOut = function(value){
		if(sessionStorage.getItem('value_out') != null){
			sessionStorage.removeItem('value_out');
			sessionStorage.setItem('value_out',value);
		}
	}
});	