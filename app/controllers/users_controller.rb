class UsersController < ApplicationController

  before_action :authenticate_admin, :except => [:edit]
  
  def index
    @users = User.order("role").order("full_name")
  end

  def new 
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Nuevo usuario creado."
      redirect_to users_path
    else
      if @user.errors.keys == [:email]
        flash[:notice] = "Correo electrónico está en uso."  
      end      
      render :new
    end
  end

  def edit
    if current_user.id == params[:id].to_i
      @user = User.find(params[:id])
    else
      if current_user.role == "admin"
        @user = User.find(params[:id])
      else 
        redirect_to root_path
      end
    end
  end

  def update
    @user = User.find(params[:id])
    if params[:user][:password].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
    end
    if @user.update_attributes(user_params)
      flash[:success] = "Usuario editado satisfactoriamente."
      redirect_to "/admin/users/#{@user.id}/edit"
    else
      if @user.errors.keys == [:email]
        flash[:notice] = "Correo electrónico está en uso."  
      end
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    if params[:opt] == "1"
      @user.is_active = false
      if @user.save
        flash[:success] = "Usuario bloqueado satisfactoriamente."
      else
        flash[:notice] = "No se pudo bloquear este usuario."  
      end
    else
      @user.is_active = true
      if @user.save
        flash[:success] = "Usuario desbloqueado satisfactoriamente."
      else
        flash[:notice] = "No se pudo desbloquear este usuario."  
      end
    end
    redirect_to users_path
  end

  private

  def user_params
    params.require(:user).permit(:role, :full_name, :colombian_id, :email, :password, :password_confirmation, :created_by)
  end

end
