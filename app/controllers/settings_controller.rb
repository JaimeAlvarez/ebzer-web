class SettingsController < ApplicationController
  before_action :authenticate_admin
  include SettingHelper
  
  def index
    $FIND_DEVICES = ''
    @parking_cameras = ParkingCamera.all
  	@parking_setting = ParkingSetting.first
    @ref_cameras = RefCamera.all
    @count_ref = RefCamera.count
  end
    
  def update
  	@parking_setting = ParkingSetting.find(params[:id])
    respond_to do |format|
      format.json{
        if @parking_setting.update_attributes(parking_settings_params)
          render json: @parking_setting, status: :ok
        else
          render json: @parking_setting.errors.to_json, status: 422
        end
      }
    end
  end

  def get_conf_manager
    conf_manager = ConfManager.first
    render json: conf_manager
  end

  def conf_manager
    config_manager = ConfManager.first
    
    if config_manager.update(conf_manager_params)
      
      ip = params[:conf_manager][:ip]
      mask = params[:conf_manager][:mask]
      gateway = params[:conf_manager][:gateway]
      dns_one = params[:conf_manager][:dns_one]
      dns_two = params[:conf_manager][:dns_two]
      
      network_conf(ip,mask,gateway,dns_one,dns_two)

      render json: config_manager, status: :ok
    else
      render json: config_manager.errors.to_json, status: 422
    end
  end

  def path_result
    $FIND_DEVICES = find_device
    render json: $FIND_DEVICES
  end

  def reboot_system
    render json: "shutdown"
    system("sudo reboot")
  end

  def backup_usb

    nombre = Time.now.getutc.to_i
    device = params[:device]
    response_cp = false

    path = "/media/odroid/#{device}" 
    system "mkdir #{path}/EbzerParking_Backup#{nombre}"

    if($FIND_DEVICES != nil and device != nil)
      response_cp  = system "cp -r /home/odroid/ebzer-web/public/photos /#{path}/EbzerParking_Backup#{nombre}"
      cmd = "sudo pg_dump ebzerweb_production -h localhost -U postgres > #{path}/EbzerParking_Backup_#{nombre}/backup_#{nombre}.sql"
      response = system(cmd)
    elsif device == nil
      response = "No hay dispositivo"
    else
      response = "Se han desconectado dispositivo\nPor favor de click en Refrescar"
    end
    
    render json: {
      :response => response,
      :response_cp => response_cp
    }
  end

  def show
    @parking_setting = ParkingSetting.find(params[:id])
    render json: @parking_setting
  end

  def drop_tables
    BlackList.delete_all
    WhiteList.delete_all
    Entry.delete_all
    render json: "Ok."
  end
  
  private
  def parking_settings_params
  	params.require(:parking_setting).permit(:name, :maximum_capacity, :number_of_entries, :observations, :owner, :location, :number_of_cameras)
  end
  
  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "ebenezer" && password == "123456"
    end
  end

  def conf_manager_params
    params.require(:conf_manager).permit(:ip,:mask,:gateway,:dns_one,:dns_two)
  end
end
