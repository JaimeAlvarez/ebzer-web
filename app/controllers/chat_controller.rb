class ChatController < WebsocketRails::BaseController
  def initialize_session
    # perform application setup here
    controller_store[:message_count] = 0
  end
  
  def new_entries
  	new_message = {:message => 'this is a message'}
	  send_message :entries, new_message
  end

  def other_entries
  end
end