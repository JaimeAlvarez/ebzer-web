class WhiteListsController < ApplicationController
  before_action :authenticate_user!

  def index
  	@white_lists = WhiteList.paginate(per_page: 50, page: params[:page])
  	@white_list = WhiteList.new

    respond_to do |format|
      format.html{}
      format.xlsx{
        @white_lists = WhiteList.all
        render xlsx: "index", disposition: "attachment", filename: "Lista_Blanca.xlsx"
      }
    end
  end

  def new
  end

  def search_b
    count_b = BlackList.all.where(plate: params[:id]).count
    render :text => count_b.to_json
  end

  def create
  	@white_list = WhiteList.new(white_list_params)
    count_w = WhiteList.all.where(plate: params[:white_list][:plate]).count
    search_b = params[:search_b]

    if search_b == "true"
      black_plate = BlackList.find_by(plate: params[:white_list][:plate])
      black_plate.destroy
    end
    
    respond_to do |format|
	    if count_w == 0
		    if @white_list.save
			    format.json{render json: @white_list, status: :ok}
		    else
			    format.json{render json: "Error.", status: :unprocessable_entity}
		    end
	    else
		    format.json{render json: "Placa ya está en Base de Datos.", status: :unprocessable_entity}
	    end
	  end
  end

  def destroy
    @white_list = WhiteList.find(params[:id])
    
    respond_to do |format|
      if @white_list.destroy
        format.json{render json: 'ok', status: :created}
      else
        format.json{render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def detected
    @entries = Entry.where("plate_chars in (SELECT plate FROM white_lists)").paginate(per_page: 100, page: params[:page])
  end

  def update
    edit_plate = WhiteList.find(params[:id_placa_edit])
    edit_plate.plate = params[:edit_plate]
    edit_plate.motivo = params[:edit_motivo]

    respond_to do |format|
 	if edit_plate.save
	   format.json{render json: edit_plate, status: :ok}
	else
	   format.json{render json: "Error.", status: :unprocessable_entity}
	end
    end 
  end

  def search
    @plate = WhiteList.find_by(plate: params[:plate])

    respond_to do |format|
      if !@plate.nil?
        format.json{render json: @plate, status: :ok}
      else
        format.json{render json: 'Placa no encontrada.', status: :ok}
      end
    end
  end


  private
  def white_list_params
    params.require(:white_list).permit(:plate, :reason)
  end
  
  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "ebenezer" && password == "123456"
    end
  end
end
