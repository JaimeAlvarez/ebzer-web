class EntriesController < ApplicationController
  skip_before_filter :verify_authenticity_token
  #before_filter :authenticate, only: [:index]
  before_action :authenticate_user!, except: [:create]
  def index
    @entries = Entry.paginate(per_page: 100, page: params[:page])    
    respond_to do |format|
      format.html{}
      format.json{render json: @entries, status: :ok}
      format.xlsx {
        @entries = Entry.all
        render xlsx: "index", disposition: "attachment", filename: "Entradas.xlsx"
      }
    end
  end

  def create
  	entry = Entry.new(entry_params)

  	respond_to do |format|
  		temp = Entry.where(plate_chars: entry.plate_chars, entry_type: entry.entry_type, created_at: 90.seconds.ago..3.seconds.ago).count
      
      sw = false
      if temp < 3
        sw = true
      end

      if sw
        if entry.save          
          if entry.entry_type == 1 || entry.entry_type == 2
            if BlackList.where(plate: entry.plate_chars).count('id') > 0
              WebsocketRails[:real_broadcast].trigger('black_list', EntrySerializer.new(entry))            
            elsif WhiteList.where(plate: entry.plate_chars).count('id') > 0
              WebsocketRails[:real_broadcast].trigger('white_list', EntrySerializer.new(entry))
              if entry.entry_type == 1
		            %x['/home/odroid/Documents/199']
              end
              if entry.entry_type == 2
                %x['/home/odroid/Documents/200']
              end
            else
              WebsocketRails[:real_broadcast].trigger('other_entries', EntrySerializer.new(entry))
	            if entry.entry_type == 2
                %x['/home/odroid/Documents/200']
              end
            end            
          end #end entry ||

          format.json{render json: entry, status: :ok}  
        else
          format.json{render json: entry.errors.to_json, status: :unprocessable_entity}
        end
      else
        format.json{render json: "REPEATED".to_json, status: :ok}
      end
    end #end repond
  end

  def update
    @entry = Entry.find(params[:id])
    @entry.edited = true

    respond_to do |format|
      format.json{
        if @entry.update_attributes(entry_params_update)
          render json: @entry, status: :ok
        else
          render json: @entry.errors.to_json, status: 422
        end
      }
    end
  end

  def search
    @plate_chars = params[:plate_chars]
    @initial_date = params[:initial_date]
    @final_date = params[:final_date]

    if @initial_date != ""
      initial_date_format = @initial_date.split('/')
      initial_date = Date.new(initial_date_format[2].to_i, initial_date_format[1].to_i, initial_date_format[0].to_i)
      initial_date = initial_date.beginning_of_day
    end

    if @final_date != ""
      final_date_format = @final_date.split('/')
      final_date = Date.new(final_date_format[2].to_i, final_date_format[1].to_i, final_date_format[0].to_i)
      final_date = final_date.end_of_day
    end

    if @plate_chars != "" && @initial_date == "" && @final_date == ""
      @entries = Entry.where("plate_chars = ?", @plate_chars).paginate(per_page: 100, page: params[:page])
      @entries_xls = Entry.where("plate_chars = ?", @plate_chars)
    end

    if @plate_chars != "" && @initial_date != "" && @final_date == ""
      @entries = Entry.where("plate_chars = ? AND creation_date_at_device >= ?", @plate_chars, initial_date).paginate(per_page: 100, page: params[:page])
      @entries_xls = Entry.where("plate_chars = ? AND creation_date_at_device >= ?", @plate_chars, initial_date)
    end

    if @plate_chars != "" && @initial_date != "" && @final_date != ""
        @entries = Entry.where("plate_chars = ? AND creation_date_at_device >= ? AND creation_date_at_device <= ?", @plate_chars, initial_date, final_date).paginate(per_page: 100, page: params[:page])
        @entries_xls = Entry.where("plate_chars = ? AND creation_date_at_device >= ? AND creation_date_at_device <= ?", @plate_chars, initial_date, final_date)
    end

    if @plate_chars == "" && @initial_date != "" && @final_date != ""
      @entries = Entry.where("creation_date_at_device >= ? AND creation_date_at_device <= ?", initial_date, final_date).paginate(per_page: 100, page: params[:page])
      @entries_xls = Entry.where("creation_date_at_device >= ? AND creation_date_at_device <= ?", initial_date, final_date)
    end

    if @plate_chars == "" && @initial_date == "" && @final_date != ""
        @entries = Entry.where("creation_date_at_device <= ?", final_date).paginate(per_page: 100, page: params[:page])
        @entries_xls = Entry.where("creation_date_at_device <= ?", final_date)
    end

    if @plate_chars != "" && @initial_date == "" && @final_date != ""
        @entries = Entry.where("plate_chars = ? AND creation_date_at_device <= ?", @plate_chars, final_date).paginate(per_page: 100, page: params[:page])
        @entries_xls = Entry.where("plate_chars = ? AND creation_date_at_device <= ?", @plate_chars, final_date)
    end

    if @plate_chars == "" && @initial_date != "" && @final_date == ""
        @entries = Entry.where("creation_date_at_device >= ?", initial_date).paginate(per_page: 100, page: params[:page])
        @entries_xls = Entry.where("creation_date_at_device >= ?", initial_date)
    end

    if @plate_chars == "" && @initial_date == "" && @final_date == "" 
      @entries = Entry.all.paginate(per_page: 100, page: params[:page])
      @entries_xls = Entry.all
    end
    respond_to do |format|
      format.html{}
      format.xlsx {
        render xlsx: "search", disposition: "attachment", filename: "busqueda.xlsx"
      }
    end
  end

  def edited
    @entries = Entry.where(edited: true).paginate(per_page: 100, page: params[:page])
  end

  private

  def entry_params
    temp = params.permit(:device_id, :plate_chars, :plate_segment, :creation_date, :entry_type, full_image: [:data,:ext,:name], plate_segment: [:data,:ext,:name])
    r = request.url.split('/')
    temp[:server_path] = r.size > 4 ? r[2]+'/'+r[3] : r[2] 
    temp
  end

  def entry_params_update
    params.require(:entry).permit(:plate_chars)
  end

  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "ebenezer" && password == "123456"
    end
  end
end
