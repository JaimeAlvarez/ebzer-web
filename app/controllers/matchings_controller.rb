class MatchingsController < ApplicationController

  before_action :authenticate_user!
  
  def index
  	@entries = Entry.where(entry_related_id: nil).paginate(per_page: 100, page: params[:page])
  	@entry = Entry.new
  end

  def update
  	@entry = Entry.find(params[:id])

  	entry_id = params[:entry_id]

  	respond_to do |format|
  		format.json{
  			if @entry.update_attributes(entry_related_id: entry_id)
  				render json: @entry, status: :ok
  			else
  				render json: @entry.errors.to_json, status: 422
				end
  		}
		end
  end

  def show
  	@entry = Entry.find(params[:id])
  	entry_type_id_find = @entry.entry_type == 1 ? 2 : 1
  	@entries = Entry.where(plate_chars: @entry.plate_chars, entry_type: entry_type_id_find)

  	render partial: 'entries_to_match'
  end
end