class ParkingCamerasController < ApplicationController
	before_action :authenticate_admin, only: [:create, :update, :destroy]
  
	def index
		@parking_cameras = ParkingCamera.all
  	@id_parking_cameras_in = ParkingCamera.where(type_of: 'Entrada', state: true).pluck(:id)
    @id_parking_cameras_out = ParkingCamera.where(type_of: 'Salida', state: true).pluck(:id)
    @parking_cameras_in_first = ParkingCamera.where(type_of: 'Entrada', state: true).pluck(:description).first
    @parking_cameras_out_first = ParkingCamera.where(type_of: 'Salida', state: true).pluck(:description).first
    
    @count = @parking_cameras.count
    @count_in = @id_parking_cameras_in.count
    @count_out = @id_parking_cameras_out.count

    render json: {
      :parking_cameras => @parking_cameras, 
      :number_parking_cameras_in => @count_in,
      :number_parking_cameras_out => @count_out,
      :id_parking_cameras_in => @id_parking_cameras_in,
      :id_parking_cameras_out => @id_parking_cameras_out,
      :parking_cameras_in_first => @parking_cameras_in_first,
      :parking_cameras_out_first => @parking_cameras_out_first,
      :count => @count
    }
  end

	def create
	  @ref_camera = RefCamera.find(params[:parking_camera][:ref_camera])
    @parking_camera = ParkingCamera.new(parking_camera_params)
    @parking_camera.ref_camera_id = @ref_camera.id #error
    
    if @parking_camera.save
      render json: @parking_camera, status: :ok
    else
      render json: @parking_camera, status: 422
    end
  end

  def show
  	@parking = ParkingCamera.find(params[:id])
		render json: @parking
  end

  def update
    @ref_camera = RefCamera.find(params[:parking_camera][:ref_camera])
    @parking_camera = ParkingCamera.find(params[:id])
    @parking_camera.ref_camera_id = @ref_camera.id #error

    if @parking_camera.update(parking_camera_params)
      render json: @parking_camera, status: :ok
    else
      #render json: @parking_camera, status: :422
    end
  end

  def destroy
    parking = ParkingCamera.find(params[:id])
    if parking.delete
       render json: parking, status: :ok
    else
       render json: parking
    end
  end

  private
	  def parking_camera_params
      params.require(:parking_camera).permit(:state,:user,:password,:ip,:port,:type_of,:description, :id, :protocol)
    end
end
