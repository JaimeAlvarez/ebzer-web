class BrandsController < ApplicationController
	before_action :authenticate_admin
	
	def show
		@brand = Brand.find(params[:id])
		render json: @brand
	end

end
