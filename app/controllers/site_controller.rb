class SiteController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @parking_setting = ParkingSetting.first
    @parking_cameras_in = ParkingCamera.where(type_of: 'Entrada', state: true)
    @parking_cameras_out = ParkingCamera.where(type_of: 'Salida', state: true)
  	
    @entry = Entry.new
    @entries = Entry.where(edit: true, edited: nil)
  	
    @entry_type_1 = Entry.where(entry_type: 1)[0]
  	@entry_type_2 = Entry.where(entry_type: 2)[0]

    if !@entry_type_1.nil?
    	if WhiteList.where('plate = ?', @entry_type_1.plate_chars.to_s).count('id') > 0
    		@state_input = "Acceso permitido."
    	else
    		@state_input = "Acceso denegado."
    	end
    end
    if !@entry_type_2.nil?
      if WhiteList.where('plate = ?', @entry_type_2.plate_chars.to_s).count('id') > 0
        @state_output = "Acceso permitido."
      else
        @state_output = "Acceso denegado."
      end
    end
  	#@entry_type_1 = nil
  	#@entry_type_2 = nil
  end

  def create_manual
    require 'open-uri'
    entry = Entry.new(manual_params)
    entry.creation_date_at_device = Time.now
    entry.plate_segment_url = ""
    time = Time.now.to_s
    entry.full_image_url = Rails.root.join('/photos/manual_entry_'+time+'.jpg')
    if entry.entry_type == 1
    	file = open($URL_CAPTURE_INPUT).read
    end
    if entry.entry_type == 2
	    file = open($URL_CAPTURE_OUTPUT).read
    end
    #file.force_encoding("utf-8")
    File.write('/home/odroid/Documents/ebzer-web/public/photos/manual_entry_'+time+'.jpg', file)
    respond_to do |format|
      if entry.save
        if entry.entry_type == 1 || entry.entry_type == 2
          if BlackList.where(plate: entry.plate_chars).count('id') > 0
            WebsocketRails[:real_broadcast].trigger('black_list', EntrySerializer.new(entry))            
          elsif WhiteList.where(plate: entry.plate_chars).count('id') > 0
            WebsocketRails[:real_broadcast].trigger('white_list', EntrySerializer.new(entry))
          else
            WebsocketRails[:real_broadcast].trigger('other_entries', EntrySerializer.new(entry))
          end

	  if entry.entry_type == 1
	          %x['/home/odroid/Documents/199']
          end
          if entry.entry_type == 2
        	  %x['/home/odroid/Documents/200']
          end
        end
      end
      format.json{render json: entry, status: :ok}
    end #end repond
  end #Create manual

  private
  def manual_params
    params.require(:entry).permit(:id, :device_id, :full_image_url, :plate_chars, :entry_type, :plate_segment_url, :creation_date_at_device, :image)
  end
end
