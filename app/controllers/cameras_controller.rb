class CamerasController < ApplicationController
	before_action :authenticate_admin, only: [:create,:update,:index]
	#REF_CAMERA
	#REF_CAMERA
	def index
		@ref_cameras = RefCamera.all
	end
	
	def create
		brand = Brand.find_or_create_by(brand: params[:refcam][:brand])

		@ref_camera = RefCamera.new(ref_camera_params)
		@ref_camera.brand = brand
	
		respond_to do |format|
	        format.json{
		        if @ref_camera.save!
				  render json: @ref_camera, status: :ok
		        else
		          render json: @ref_camera.errors
		        end
	    	}
		end
	end

	def show
		@ref_camera = RefCamera.find(params[:id])
		idcam = @ref_camera.brand_id 
		brand = Brand.find(idcam) 

        render json:{
	        	:ref_camera => @ref_camera,
	        	:brand => brand
        			}, status: :ok
	end

	def update
		brand = Brand.find_or_create_by(brand: params[:refcam][:brand])
		@ref_camera = RefCamera.find(params[:id])
		@ref_camera.brand = brand

		respond_to do |format|
	        format.json{
		        if @ref_camera.update_attributes(ref_camera_params)
		          render json: @ref_camera, status: :ok
		        else
		          render json: @ref_camera.errors.to_json, status: 422
		        end
	      	}
        end
	end

	private
		def ref_camera_params
			params.require(:refcam).permit(:reference,:path)
		end
end
