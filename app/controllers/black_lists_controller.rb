class BlackListsController < ApplicationController
  before_action :authenticate_user!

  def index
  	@black_lists = BlackList.paginate(per_page: 100, page: params[:page])
  	@black_list = BlackList.new

    respond_to do |format|
      format.html{}
      format.xlsx{
        @black_lists = BlackList.all
        render xlsx: "index", disposition: "attachment", filename: "Listas_Negras.xlsx"
      }
    end
  end

  def new
  end

  def search_w
    count_w = WhiteList.all.where(plate: params[:id]).count
    render :text => count_w.to_json
  end

  def create
  	@black_list = BlackList.new(black_list_params)
    count_b = BlackList.all.where(plate: params[:black_list][:plate]).count 
    search_w = params[:search_w]
    
    if search_w == "true"
      white_plate = WhiteList.find_by(plate: params[:black_list][:plate])
      white_plate.destroy
    end
    
    respond_to do |format|
      if count_b == 0
        if @black_list.save
          format.json{render json: @black_list, status: :ok}
        else
          format.json{render json: 'Error', status: :unprocessable_entity}
        end
      else
        format.json{render json: "Placa ya está en Base de Datos.", status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @black_list = BlackList.find(params[:id])    
    respond_to do |format|
      if @black_list.destroy
        format.json{render json: 'ok', status: :created}
      else
        format.json{render json: 'error', status: :unprocessable_entity}
      end
    end
  end

  def detected
    @entries = Entry.where("plate_chars in (SELECT plate FROM black_lists)").paginate(per_page: 100, page: params[:page])
  end


  private
  def black_list_params
    params.require(:black_list).permit(:plate, :reason)
  end
  
  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      username == "ebenezer" && password == "123456"
    end
  end
end
