class ParkingSetting < ActiveRecord::Base
	#Validations
	validates :name, presence: true
	validates :number_of_entries, :maximum_capacity, numericality: true

	#Callbacks
	before_save :before_save_callback

	private
	def before_save_callback
		self.available_capacity = self.maximum_capacity - self.number_of_entries
	end
end
