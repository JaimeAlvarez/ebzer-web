class WhiteList < ActiveRecord::Base
	#Default_Scope
	default_scope{order('plate asc')}
	
	#Validations
	validates :plate, presence: true

	#Callbacks
	before_save :before_save_callback

	private
	def before_save_callback
		self.plate = plate.upcase
	end
end
