class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :registable, :trackable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

  def active_for_authentication?
  	super and self.is_active?	
  end
end
