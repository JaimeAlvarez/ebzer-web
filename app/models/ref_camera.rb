class RefCamera < ActiveRecord::Base
	belongs_to :brand

	validates :reference, uniqueness: true
end