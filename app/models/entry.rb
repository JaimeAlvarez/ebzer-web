class Entry < ActiveRecord::Base
	attr_accessor :plate_segment, :creation_date, :full_image, :server_path

	#Default Scope
	default_scope{order(created_at: :desc)}
	
	#Callbacks
	after_validation :after_validation_callback
	before_create :after_validation_callback
	before_save :before_save_callback
	after_save :after_save_callback

	def entry_type_str
		if self.entry_type.nil?
			''
		else
			if self.entry_type == 1
				'Entrada'
			elsif self.entry_type == 2
				'Salida'
			end
		end
	end

	def date
		self.creation_date_at_device.strftime('%d/%m/%Y')
  	end

	def hours
		self.creation_date_at_device.strftime('%H:%M')
  end

  def time
  	if entry_type == 2
  		if !self.time_related.nil?
  			Time.diff(self.time_related, self.creation_date_at_device,'%h:%m:%s')[:diff]
			else
				'N-R'
			end
		else
			''
		end
	end

	def edited_str
		if self.edited.nil?
			''
		else
			if self.edited == true
				'Si'
			else
				'No'
			end
		end
	end

	def maximun_capacity
		parking_setting = ParkingSetting.first
		parking_setting.maximun_capacity
	end

	def available_capacity
		parking_setting = ParkingSetting.first
		parking_setting.available_capacity.nil? ? parking_setting.maximun_capacity : parking_setting.capacidad_disponible
	end

	def number_of_entries
		parking_setting = ParkingSetting.first
		parking_setting.number_of_entries.nil? ? 0 : parking_setting.number_of_entries
	end

	def need_to_edit
		self.plate_chars.include?('@')
	end

	def reason_black_list
		black_list = BlackList.where(plate: self.plate_chars)[0]
		
		if black_list.nil?
			''
		else
			black_list.motivo
		end
	end

	def reason_white_list
		white_list = WhiteList.where(plate: self.plate_chars)[0]
		
		if white_list.nil?
			''
		else
			white_list.motivo
		end 
	end

	private
	def before_save_callback
		if self.entry_type == 2
			entry_related = Entry.where(plate_chars: self.plate_chars, entry_type: 1, entry_related_id: nil)[0]

			if entry_related
				self.entry_related_id = entry_related.id
				self.tiempo_related = entry_related.creation_date_at_device
			end
		end

		self.edited = self.plate_chars.include?('@')
		nil
	end

	def after_save_callback
		if self.entry_type == 2
			entry_related = Entry.where(id: self.entry_related_id)[0]
			
			if entry_related
				entry_related.entry_related_id = self.id
				entry_related.save
			end
		end
	end

	def after_validation_callback
		if self.plate_segment_url.nil?
			now = Time.now
			plate_segment_image_name = "segment_"+plate_segment[:name]+'_'+now.strftime('%d%m%Y%H%M%S%L')+'.'+plate_segment[:ext]
			temp_plate_segment_url = Rails.root.join('public','photos',plate_segment_image_name)
			File.open(temp_plate_segment_url, 'wb') do|f|
			  f.write(Base64.decode64(plate_segment[:data]))
			end
			self.plate_segment_url = server_path+"/photos/"+plate_segment_image_name
		end

		if self.full_image_url.nil?
			now = Time.now
			full_image_name = "full_"+full_image[:name]+'_'+now.strftime('%d%m%Y%H%M%S%L')+'.'+full_image[:ext]
			temp_full_image_url = Rails.root.join('public','photos',full_image_name)
			File.open(temp_full_image_url, 'wb') do|f|
			  f.write(Base64.decode64(full_image[:data]))
			end		
			self.full_image_url = server_path+"/photos/"+full_image_name
		end

		if self.creation_date_at_device.nil?
			self.creation_date_at_device = creation_date
		end
	end
end