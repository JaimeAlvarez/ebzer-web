class Brand < ActiveRecord::Base
	has_many :ref_cameras
	validates :brand, uniqueness: true
end