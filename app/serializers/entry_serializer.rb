class EntrySerializer < ActiveModel::Serializer
  attributes :id, :device_id, :full_image_url, :plate_segment_url, :plate_chars, :creation_date_at_device, :entry_type, :edited, :fecha, :hora, :tiempo, :capacidad_maxima, :nro_entradas, :capacidad_disponible, :necesita_editar, :entry_type_str, :motivo_black_list, :motivo_white_list

  def motivo_black_list
  	object.motivo_black_list
  end

  def motivo_white_list
  	object.motivo_white_list
  end
end