Rails.application.routes.draw do

  devise_for :users
  scope "/admin" do
    resources :users
  end

  root to: 'site#index'
  resources :entries, only: [:index, :create, :update], defaults: {format: 'json'}
  resources :messages, only: [:index]
  resources :main
  resources :matchings
  resources :settings, only:[:index, :show, :create, :update]
  resources :black_lists, except: [:show, :destroy]
  resources :white_lists, except: [:show]
  resources :cameras
  resources :parking_cameras, only:[:index, :update, :show, :destroy]
  resources :brands, only:[:show]

  put 'parking_cameras' => 'parking_cameras#update'
  post 'parking_cameras' => 'parking_cameras#create'
  post 'site/create_manual' => 'site#create_manual'
  get 'brands/all_brands' => 'brands#all_brands'
  post 'conf_manager' => 'settings#conf_manager'
  get 'get_conf_manager' => 'settings#get_conf_manager'
  post 'settings/path' => 'settings#path'
  get 'path_result' => 'settings#path_result'
  get 'drop_tables' => 'settings#drop_tables'
  post 'backup_usb' => 'settings#backup_usb'
  get 'reboot_system' => 'settings#reboot_system'

  get  'entries/search' => 'entries#search', as: 'entries_search'
  get  'entries/edited' => 'entries#edited', as: 'entries_edited'
  get  'black_lists/detected' => 'black_lists#detected', as: 'black_lists_detected'
  post 'black_lists/destroy'  => 'black_lists#destroy',  as: 'black_lists_destroy'
  get  'white_lists/detected' => 'white_lists#detected', as: 'white_lists_detected'
  post 'white_lists/search_b' => 'white_lists#search_b', as: 'white_lists_search_b'
  get  'white_lists/search'   => 'white_lists#search',   as: 'white_lists_search'
  post 'white_lists/destroy'  => 'white_lists#destroy',  as: 'white_lists_destroy'
  post 'white_lists/update'   => 'white_lists#update',   as: 'white_lists_update'
  post 'black_lists/search_w' => 'black_lists#search_w', as: 'black_lists_search_w'
  post 'settings/url_config'  => 'settings#url_config',  as: 'config_urls'
  post 'cameras/create' => 'cameras#create'

end
