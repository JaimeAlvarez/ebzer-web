SIZE="$(du ./public/photos -sm | cut -f1)"
LIMT="100"

echo "$SIZE"
echo "$LIMT"

if [ "$SIZE" -gt "$LIMT" ]; then
	echo "$(find ./public/photos/*.jpg -mtime +3 -exec rm -rf {} \;)"
fi

